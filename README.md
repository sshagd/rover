Задача: спроектировать путь ровера-беспилотника по заранее известной местности с максимальной экономией заряда.

Местность: данные в закодированном виде. Фотография, сконвертированная в матрицу с числами. Одна матрица — это прямоугольный снимок размером х на y метров.

Числа показывают высоту над уровнем моря. 0 — это высота ровно на уровне моря, а, например, 4 — это 4 единицы над уровнем моря. На Фото 1 закодирован холм, пологий слева и резко обрывающийся справа.

Ровер всегда движется из верхней левой точки [0][0] в правую нижнюю точку [N - 1][M - 1].

Ограничения:

1. Движение
Из любой точки ровер может двигаться только в четыре стороны: на север, юг, запад, восток. Ровер не может ехать по-диагонали — эта функция еще не реализована. Ровер не может вернуться в ту точку, в которой уже был.

2. Заряд
Ровер ездит на заряде. Он тратит единицу заряда на само движение, и дополнительные единицы на подъем и спуск.

3. Расход заряда
Заряд расходуется по правилу:
На 1 шаг ровер всегда тратит 1 единицу заряда. На подъем или спуск ровер тратит заряд, пропорциональный сложности подъема или спуска. Сложность подъема или спуска - это разница между высотами.

