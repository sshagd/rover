import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Rover {
    record Cell(int i, int j, int value, boolean dir){}

    public static void calculateRoverPath(int[][] map) throws IOException {
        int n = map.length;
        int m = map[0].length;
        int[][] dp = newMap(map);
        List<Cell> cells = new ArrayList<>();
        cells.add(new Cell(0, 0, map[0][0],true));
        boolean dir;
        for(int i=0; i<n; i++){
            for(int j=0; j<m; j++){
                if(i>0 && j>0){
                    dp[i][j]= Math.min(dp[i-1][j] + Math.abs(map[i][j] - map[i-1][j]) + 1,
                            dp[i][j-1] + Math.abs(map[i][j] - map[i][j-1]) + 1);
                    dir = (dp[i - 1][j] + Math.abs(map[i][j] - map[i - 1][j]) + 1) <=
                            (dp[i][j - 1] + Math.abs(map[i][j] - map[i][j - 1]) + 1);
                    cells.add(new Cell(i, j, dp[i][j], dir));
                }else{
                    if(i>0){
                        dp[i][j]= Math.abs(map[i-1][j] - map[i][j]) + dp[i-1][j] + 1;
                        cells.add(new Cell(i, j, dp[i][j], true));
                    }else if(j>0){
                        dp[i][j] = Math.abs(map[i][j-1] - map[i][j]) + dp[i][j-1] + 1;
                        cells.add(new Cell(i, j, dp[i][j], false));
                    }
                }
            }
        }
        List<Cell> steps = stepsCounter(cells, m);
        Collections.reverse(steps);
        fileWrite(steps, dp);
    }

    public static void main(String[] args) throws IOException {
//        calculateRoverPath();
    }

    static void fileWrite(List<Cell> steps, int[][] dp) throws IOException {
        int n = dp.length;
        int m = dp[0].length;
        FileWriter fw = new FileWriter("path-plan.txt");
        for(int k = 0; k < steps.size(); k ++){
            if(k < steps.size() - 1){
                fw.write("[" + steps.get(k).i + "][" + steps.get(k).j + "]->");
            } else{
                fw.write("[" + steps.get(k).i + "][" + steps.get(k).j + "]");
            }
        }
        fw.write("\nsteps: " + (steps.size()-1));
        fw.write("\nfuel: " + dp[n-1][m-1]);
        fw.close();
    }

    static ArrayList<Cell> stepsCounter(List<Cell> cells, int m){
        ArrayList<Cell> steps = new ArrayList<>();
        int i = cells.size() - 1;
        while(i >= 0 ){
            steps.add(cells.get(i));
            if(cells.get(i).dir()){
                i -= m;
            } else {
                i--;
            }
        }
        return steps;
    }

    static int[][] newMap(int[][] map){
        int[][] d = new int[map.length][map[0].length];
        for (int i = 0; i < d.length; i++) {
            d[i] = Arrays.copyOf(map[i], map[i].length);
        }
        return d;
    }
}
